from client import *
import argparse

def setup_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="REST Client for Graph Library Computation Server (GLCS)")
    parser.add_argument('ip', type=str, help='IP of GLCS')
    parser.add_argument('port', type=int, help='Port of GLCS')
    return parser

if __name__ == "__main__":
    parser = setup_argparse()
    args = parser.parse_args()

    # TODO Add argument format validation
    client = GLClient(args.ip, args.port)

    allocation_name = "computation-3"
    replicas = 4

    # req = AllocateComputationRequest(allocation_name, replicas)
    # resp = client.allocate_computation(req)
    # print(resp)

    req = AllocationResultRequest(name=allocation_name)
    resp = client.allocation_result(req)
    #print(resp)

    # req = AllAllocationResultRequest()
    # resp = client.all_allocation_result(req)
    # print(resp)

    # req = DeleteAllocationRequest(name="comp-1")
    # resp = client.deallocate_computation(req)
    # print(resp)

    # Start computation on a allocated resource
    # client.glcs_ip = "10.106.88.227"
    # client.glcs_port = 8080
    computation_name = "kruskal-comp-5"
    req = StartComputationRequest(algorithm="kruskal", name=computation_name, vertexCount=1024, replicas=replicas, density=20)
    resp = client.start_computation(req)

    # Get computation result
    # req = ComputationResultRequest(name="kruskal-comp-2",)
    # resp = client.computation_result(req)
