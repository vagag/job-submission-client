import logging
import requests
import json
from dataclasses import dataclass
from typing import Final

@dataclass
class AllocateComputationRequest:
    name: str
    replicas: int

@dataclass
class AllocateComputationResponse:
    result: str

@dataclass
class AllocationResultRequest:
    name: str

@dataclass
class AllocationResultResponse:
    name: str
    replicas: int
    ip: str
    error: str

@dataclass
class AllAllocationResultRequest:
    pass

@dataclass
class AllAllocationResultResponse:
    computations: list[AllocationResultResponse]
    error: str

@dataclass
class DeleteAllocationRequest:
    name: str

@dataclass
class DeleteAllocationResponse:
    error: str

@dataclass
class StartComputationRequest:
    algorithm: str
    name: str
    vertexCount: int
    density: int
    replicas: int

@dataclass
class StartComputationResponse:
    status: str
    pass

@dataclass
class ComputationStatusRequest:
    name: str
    pass

@dataclass
class ComputationStatusResponse:
    status: str
    pass

@dataclass
class ComputationResultRequest:
    name: str
    pass

@dataclass
class ComputationResultResponse:
    status: str
    result: str
    pass

@dataclass
class ComputationStopRequest:
    name: str
    pass

@dataclass
class ComputationStopResponse:
    status: str
    pass

class GLClient:
    __glcs_default_port: Final = 30001

    __ip: str
    __port: int
    glcs_ip: str
    glcs_port: int

    def __init__(self, ip: str, port: int):
        logging.basicConfig(level=logging.INFO)
        self.__ip = ip
        self.__port = port
        self.glcs_ip = "" 
        self.glcs_port = self.__glcs_default_port

    def __get_glfs_url(self) -> str:
        return "http://{}:{}".format(self.__ip, self.__port)

    def __get_glfs_allocation_url(self) -> str:
        return "{}/allocation".format(self.__get_glfs_url())

    def __get_glfs_allocation_result_url(self) -> str:
        return "{}/result".format(self.__get_glfs_allocation_url())

    def __get_glfs_allocation_result_all_url(self) -> str:
        return "{}/all".format(self.__get_glfs_allocation_result_url())

    def __get_glfs_allocation_delete_url(self) -> str:
        return "{}".format(self.__get_glfs_allocation_url())

    def __get_glcs_url(self) -> str:
        return "http://{}:{}".format(self.glcs_ip, self.glcs_port)

    def __get_glcs_computation_url(self) -> str:
        return "{}/computation".format(self.__get_glcs_url())

    def __get_glcs_computation_start_url(self) -> str:
        return "{}/start".format(self.__get_glcs_computation_url())
    
    def __get_glcs_computation_status_url(self) -> str:
        return "{}/status".format(self.__get_glcs_computation_url())

    def __get_glcs_computation_result_url(self) -> str:
        return "{}/result".format(self.__get_glcs_computation_url())

    def __get_glcs_computation_stop_url(self) -> str:
        return "{}/stop".format(self.__get_glcs_computation_url())

    def __convert_allocation_result_to_response(self, computation) -> AllocationResultResponse:
        return AllocationResultResponse(name=computation.get('name'), replicas=computation.get('replicas'), ip=computation.get('ip'), error="")

    def allocate_computation(self, req: AllocateComputationRequest) -> AllocateComputationResponse:
        logging.info("Allocating computation resources with name={} and replicas={}. URL={}".format(req.name, req.replicas, self.__get_glfs_allocation_url()))

        payload = {}
        payload['name'] = req.name
        payload['replicas'] = req.replicas

        try:
            resp = requests.post(self.__get_glfs_allocation_url(), data=json.dumps(payload)).json()
            logging.info("Got response={}".format(resp))
            return AllocateComputationResponse(result=resp.get('result'))
        except BaseException as err:
            print(f"Unexpected {err=}")

        return AllocateComputationResponse(result='')
    
    def allocation_result(self, req: AllocationResultRequest) -> AllocationResultResponse:
        logging.info("Getting status for allocation with name={}. URL={}".format(req.name, self.__get_glfs_allocation_result_url()))

        payload = {}
        payload['name'] = req.name

        try:
            data_headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            resp = requests.get(self.__get_glfs_allocation_result_url(), data=json.dumps(payload), headers=data_headers).json()
            logging.info("Got response={}".format(resp))

            computation = resp.get('computation')
            if computation:
                ip = computation.get('ip')
                if ip:
                    logging.info("Updating GLCS IP to {}".format(ip))
                    self.glcs_ip = ip
                return AllocationResultResponse(name=computation.get('name'), replicas=computation.get('replicas'), ip=ip, error="")

            return AllocationResultResponse(name="", replicas=0, ip="", error=computation.get('error'))
        except BaseException as err:
            print(f"Unexpected {err=}")

        return AllocationResultResponse(name="", replicas=0, ip="", error='error occured')

    def all_allocation_result(self, _: AllAllocationResultRequest) -> AllAllocationResultResponse:
        logging.info("Getting status for all allocations. URL={}".format(self.__get_glfs_allocation_result_all_url()))

        payload = {}

        resp = requests.get(self.__get_glfs_allocation_result_all_url(), data=json.dumps(payload)).json()
        logging.info("Got response={}".format(resp))
        return AllAllocationResultResponse([self.__convert_allocation_result_to_response(r) for r in resp.get('computations')], error="")

    def deallocate_computation(self, req: DeleteAllocationRequest) -> DeleteAllocationResponse:
        logging.info("Deleting allocation with name={}. URL={}".format(req.name, self.__get_glfs_allocation_delete_url()))

        payload = {}
        payload['name'] = req.name

        resp = requests.delete(self.__get_glfs_allocation_delete_url(), data=json.dumps(payload)).json()
        logging.info("Got response={}".format(resp))
        return DeleteAllocationResponse(error=resp.get('error'))

    def start_computation(self, req: StartComputationRequest):
        logging.info("Starting new computation with name={}. URL={}".format(req.name, self.__get_glcs_computation_start_url()))

        payload = {}
        payload['algorithm'] = req.algorithm
        payload['name'] = req.name
        payload['vertexCount'] = req.vertexCount
        payload['density'] = req.density
        payload['replicas'] = req.replicas

        resp = requests.post(self.__get_glcs_computation_start_url(), data=json.dumps(payload)).json()
        logging.info("Got response={}".format(resp))

        status = resp.get('status')
        if status:
            return ComputationStatusResponse(status=status.get('status'))

        return StartComputationResponse(status="")

    def computation_status(self, req: ComputationStatusRequest):
        logging.info("Getting status for computation with name={}, URL={}".format(req.name, self.__get_glcs_computation_status_url()))

        payload = { }
        payload['name'] = req.name

        resp = requests.get(self.__get_glcs_computation_status_url(), data=json.dumps(payload)).json()
        status = resp.get('status')
        logging.info("Got response={}".format(resp))

        if status:
            return ComputationStatusResponse(status=status.get('status'))

        return ComputationStatusResponse(status=resp.get('status'))

    def computation_result(self, req: ComputationResultRequest):
        logging.info("Getting status for computation with name={}, URL={}".format(req.name, self.__get_glcs_computation_result_url()))

        payload = { }
        payload['name'] = req.name

        resp = requests.get(self.__get_glcs_computation_result_url(), data=json.dumps(payload)).json()
        logging.info("Got response={}".format(resp))

        result = resp.get('result')
        if result:
            return ComputationResultResponse(status=result.get('status'), result=result.get('result'))

        return ComputationResultResponse(status="", result="")

    def end_computation(self):
        pass

